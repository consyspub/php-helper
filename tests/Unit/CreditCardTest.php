<?php

declare(strict_types=1);

namespace Tests\Unit;

use Consys\Helper\CreditCard;
use Consys\Helper\Tests\TestCase;
use Consys\Helper\Types\CardFlag;

class CreditCardTest extends TestCase
{
    public function testGetFlagReturnsCorrectFlag()
    {
        $this->assertEquals(
            CardFlag::Mastercard,
            (new CreditCard('5148953922445897'))->getFlag()
        );
        $this->assertEquals(
            CardFlag::Mastercard,
            (new CreditCard('5378 4017 8573 7506'))->getFlag()
        );
        $this->assertEquals(
            CardFlag::Visa,
            (new CreditCard('4757 8586 6932 0507'))->getFlag()
        );

        $this->assertEquals(
            CardFlag::Diners,
            (new CreditCard('3810 431719 2140'))->getFlag()
        );
        $this->assertEquals(
            CardFlag::Elo,
            (new CreditCard('6363 6873 9201 1963'))->getFlag()
        );
        $this->assertEquals(
            CardFlag::Amex,
            (new CreditCard('3771 411115 39778'))->getFlag()
        );
        $this->assertEquals(
            CardFlag::Discover,
            (new CreditCard('6011 7480 2232 3727'))->getFlag()
        );
        $this->assertEquals(
            CardFlag::Aura,
            (new CreditCard('5038 5269 9105 2399'))->getFlag()
        );
        $this->assertEquals(
            CardFlag::Jcb,
            (new CreditCard('3507 3307 1356 0764'))->getFlag()
        );
        $this->assertEquals(
            CardFlag::Hipercard,
            (new CreditCard('6062 8211 6101 0815'))->getFlag()
        );
    }
}
