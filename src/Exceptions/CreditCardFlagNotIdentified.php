<?php

declare(strict_types=1);

namespace Consys\Helper\Exceptions;

class CreditCardFlagNotIdentified extends \Exception
{
}
