<?php
namespace ConsysHelper\Factory;

use ConsysHelper\ConsysHelperFormatter;

class TextoPuroHelper implements ConsysHelperFormatter {
    public function format(string $input) : string {
        return preg_replace('/\d/', '', $input);
	}
}