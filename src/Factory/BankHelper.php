<?php
namespace ConsysHelper\Factory;

use ConsysHelper\ConsysHelperFormatter;

class BankHelper implements ConsysHelperFormatter {
    public function format(string $input) : string {
        return "";
	}

    private static function cleanBankDepositInfo(string $bankDepositInfo = null): ?int
    {
        $bankDepositInfo = preg_replace("/[Xx]/", "-0", $bankDepositInfo);
        if ($slashPosition = strpos($bankDepositInfo, '-')) {
            $bankDepositInfo = substr($bankDepositInfo, 0, $slashPosition);
        }
        return (int) $bankDepositInfo;
    }

    public static function getBankAccountDac(string $bankAccount = null): ?string
    {
        if (is_null($bankAccount)) {
            return "";
        }

        $dac = '';
        $bankAccount = preg_replace("/[Xx]/", "-0", $bankAccount);

        if (\preg_match('/\d+\-(.)/', $bankAccount, $matches)) {
            $dac = $matches[1];
        }
        $dac = preg_replace('/[^0-9]/', '', $dac);

        return $dac;
    }

    public static function getBankAgency(string $bankAgency): ?int
    {
        return self::getBankAccount($bankAgency);
    }

    public static function getBankAccount(string $bankAccount): ?int
    {
        if (is_null($bankAccount)) {
            return 0;
        }

        return self::cleanBankDepositInfo($bankAccount);
    }

    public static function getBank(string $bankCode, string $bankDescription): ?string
    {
        if (is_null($bankCode)) {
            return "";
        }

        return (string) $bankCode . "-" . $bankDescription;
    }

    private static function cleanTelefone(string $telefone): ?array
    {
        $ddd = '';
        $uncleanTel[] = $telefone;
        if (strpos($telefone, ' ')) {
            $uncleanTel = explode(' ', $telefone);
        }
        foreach ($uncleanTel as $pieceOfTel) {
            $cleanPieceOfTel = preg_replace("#[^0-9]#", "", $pieceOfTel);
            if (strlen($cleanPieceOfTel) == 2) {
                $ddd = $cleanPieceOfTel;
            } else {
                $telefones[strlen($ddd . $cleanPieceOfTel)] = $ddd . $cleanPieceOfTel;
                $ddd = '';
            }
        }
        sort($telefones);
        return $telefones;
    }

    public static function cleanMultiPhoneField(string $telefone): string
    {
        if (isset($telefone)) {
            $telefones = self::cleanTelefone($telefone);
            foreach ($telefones as $tel) {
                if (strlen($tel) >= 10 && strlen($tel) <= 12) {
                    $telWithNoZeroStart = (int) $tel;
                    return substr($telWithNoZeroStart, 0, 2) . ' ' . substr($telWithNoZeroStart, 2);
                }
            }
        }
        return "";
    }
}
