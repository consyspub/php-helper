<?php

namespace ConsysHelper\Factory;

use ConsysHelper\ConsysHelperFormatter;

class ArrayHelper implements ConsysHelperFormatter
{
    // TODO: Implement Exception
    public function format(string $input): string
    {
        return "";
    }

    /**
     * @param array $arrayKeys
     * @param string $glue
     *  Preparing joinFamiliarArrayValue to recursive call 
     * @return array
     */
    public static function toString(array $arrayKeys, $glue = ''): array
    {
        $converted = [];
        self::joinFamiliarArrayValue(
            $arrayKeys,
            $converted,
            $glue
        );
        return $converted;
    }

    /**
     * @param array $pieces
     * @param array $piecesJoined
     * @param string $glue
     * @param array|null $usedPieceName
     * This function transforms 
     *  'Pf',
     *  'Pj',
     *  'Risk' => [
     *      'High',
     *      'Med',
     *      'Low',
     *  ],
     *  'Pep'
     * to an array separeted by $glue ... [Risk,High] => RiskHigh ...
     * @return void
     */
    private static function joinFamiliarArrayValue(
        array $pieces,
        array &$piecesJoined,
        string $glue,
        ?array &$usedPieceName = []
    ): void {
        foreach ($pieces as $pieceKey => $pieceName) {
            if (!is_array($pieceName)) {
                $joinedPieces = $usedPieceName;
                array_push($joinedPieces, $pieceName);
                $piecesJoined[] = implode($glue, $joinedPieces);
            } else {
                array_push($usedPieceName, $pieceKey);
                self::joinFamiliarArrayValue(
                    $pieceName,
                    $piecesJoined,
                    $glue,
                    $usedPieceName,
                );
            }
            if ($pieceKey == array_key_last($pieces)) {
                array_pop($usedPieceName);
            }
        }
    }

    public static function implodeOrNull(string $glue, array $array): ?string
    {
        if (!count($array)) {
            return null;
        }
        return implode($glue, $array);
    }


    /**
     * @param mixed $dataFliped
     * @param mixed $newKey
     * @param mixed $fixedValue
     * @param mixed $currentValue
     * including an array in values with no content
     * when array value is empty, print row number
     * @return void
     */
    private static function setDataFliped(
        &$dataFliped,
        $newKey,
        $fixedValue,
        $currentValue
    ): void {
        $dataFliped[$newKey] = $fixedValue ?? $currentValue;
    }

    /**
     * @param array $dataToFlip
     * Prepare arrayFlipConversion recursive call
     * @return array
     */
    public function arrayFlip(array $dataToFlip): array
    {
        $dataFliped = [];
        self::arrayFlipConversion($dataToFlip, $dataFliped);
        return $dataFliped;
    }

    /**
     * @param array $dataToFlip
     * @param array $dataFliped
     * @param string $fixedValue
     * @param int $keyLevel
     * @param int $row
     * Transforms [1 => [a,b]] into [a => 1, b => 1]
     * @return void
     */
    private static function arrayFlipConversion(
        array $dataToFlip,
        array &$dataFliped = [],
        string $fixedValue = '',
        int &$keyLevel = 0,
        int &$row = 0
    ): void {
        foreach ($dataToFlip as $currentValue => $newKey) {
            if (!is_array($newKey)) {
                $keyLevel = 0;
                self::setDataFliped(
                    $dataFliped,
                    $newKey,
                    $fixedValue,
                    $currentValue
                );
            } else {
                $row++;
                if (count($newKey) == 0) {
                    $fixedValue = null;
                    self::setDataFliped(
                        $dataFliped,
                        "$row::null",
                        $fixedValue,
                        $currentValue
                    );
                    continue;
                }
                self::checkArrayCurrentLevel($keyLevel);
                self::arrayFlipConversion(
                    $newKey,
                    $dataFliped,
                    $currentValue,
                    $keyLevel,
                    $row
                );
            }
        }
    }

    private static function checkArrayCurrentLevel(&$keyLevel): void
    {
        $keyLevel++;
        if ($keyLevel > 1) {
            throw new Error("arrayFlip can only convert one-dimensional arrays.");
        }
    }
}
