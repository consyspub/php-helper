<?php
namespace ConsysHelper;

interface ConsysHelperFormatter {
	public function format(string $input) : string;
}