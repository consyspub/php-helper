<?php

declare(strict_types=1);

namespace Consys\Helper;

use Consys\Helper\Exceptions\CreditCardFlagNotIdentified;
use Consys\Helper\Types\CardFlag;

class CreditCard
{
    protected string $number;
    protected CardFlag $flag;

    public function __construct(string $number)
    {
        $this->setCardNumber($number);
    }

    protected function setCardNumber(string $number): CreditCard
    {
        $number = preg_replace('/[^0-9]/', '', $number);
        static::validateCardNumber($number);

        $this->number = $number;
        $this->flag = static::processFlag($number);

        return $this;
    }

    public function getFlag(): CardFlag
    {
        return $this->flag;
    }

    public static function processFlag(string $number): CardFlag
    {
        $eloBin = implode('|', [
            '636368',
            '438935',
            '504175',
            '451416',
            '636297',
            '506699',
            '509048',
            '509067',
            '509049',
            '509069',
            '509050',
            '09074',
            '509068',
            '509040',
            '509045',
            '509051',
            '509046',
            '509066',
            '509047',
            '509042',
            '509052',
            '509043',
            '509064',
            '509040',
        ]);

        $expressoes = [
            CardFlag::Elo->value =>
                '/^((' .
                $eloBin .
                '[0-9]{10})|(36297[0-9]{11})|(5067|4576|4011[0-9]{12}))/',
            CardFlag::Discover->value =>
                '/^((6011[0-9]{12})|(622[0-9]{13})|(64|65[0-9]{14}))/',
            CardFlag::Diners->value =>
                '/^((301|305[0-9]{11,13})|(36|38[0-9]{12,14}))/',
            CardFlag::Amex->value => '/^((34|37[0-9]{13}))/',
            CardFlag::Hipercard->value => '/^((38|60[0-9]{11,17}))/',
            CardFlag::Aura->value => '/^((50[0-9]{14}))/',
            CardFlag::Jcb->value => '/^((35[0-9]{14}))/',
            CardFlag::Mastercard->value => '/^((5[0-9]{15}))/',
            CardFlag::Visa->value => '/^((4[0-9]{12,15}))/',
        ];

        foreach ($expressoes as $bandeira => $expressao) {
            if (preg_match($expressao, $number)) {
                return CardFlag::from($bandeira);
            }
        }

        throw new CreditCardFlagNotIdentified();
    }

    public static function validateCardNumber(string $number): void
    {
        // TODO: implement proper card number validation
        if (!$number) {
            throw new \Exception('card number empty');
        }

        if (strlen($number) < 13 || strlen($number) > 19) {
            throw new \Exception('card number: invalid length');
        }
    }
}
