<?php

declare(strict_types=1);

namespace Consys\Helper\Types;

enum CardFlag: string
{
    case Mastercard = 'mastercard';
    case Visa = 'visa';
    case Diners = 'diners';
    case Elo = 'elo';
    case Amex = 'amex';
    case Discover = 'discover';
    case Aura = 'aura';
    case Jcb = 'jcb';
    case Hipercard = 'hipercard';
}
