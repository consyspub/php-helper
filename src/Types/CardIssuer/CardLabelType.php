<?php

namespace ConsysHelper\CardIssuer\Types;

use ConsysHelper\ConsysHelper;

class CardLabelType
{
    const ISSUE_LABEL = [
        'Visa' => [
            4
        ],
        'Mastercard' => [
            5
        ],
        'Diners' => [
            301,
            305,
            36,
            38
        ],
        'Elo' => [
            636368,
            509048,
            509067,
            509049,
            509069,
            509050,
            509074,
            509068,
            509040,
            509045,
            509051,
            509046,
            509066,
            509047,
            509042,
            509052,
            509043,
            509064,
            509040,
            504175,
            451416,
            438935,
            36297,
            5067,
            4576,
            4011
        ],
        'Amex' => [
            34,
            37
        ],
        'Discover' => [
            6011,
            622,
            64,
            65
        ],
        'Aura' => [
            50
        ],
        'jcb' => [
            35
        ],
        'Hipercard' => [
            38,
            60
        ],
    ];

    protected string $label;

    public function __construct(protected int $cardNumber)
    {
        $cardNumber = (int) substr($cardNumber, 0, 6);
        $this->cardPrefixNumber = $cardNumber;
        $this->setLabel($cardNumber);
    }

    private function getLabelFromCardNumber($cardNumber)
    {
    }

    /**
     * Get the value of cardPrefixNumber
     */
    public function getCardPrefixNumber()
    {
        return $this->cardPrefixNumber;
    }

    /**
     * Get the value of label
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set the value of label
     *
     * @return  self
     */
    public function setLabel($cardNumber)
    {
        $arrayHelper = ConsysHelper::factory('array');
        $flipedLabels = $arrayHelper::arrayFlip(static::ISSUE_LABEL);

        $stringlength = 6;
        do {
            $label = self::findCardNumberLabel(
                $cardNumber,
                $flipedLabels,
                $stringlength
            );
            $stringlength--;
        } while (!$label && $stringlength > 0);

        $this->label = $label;

        return $this;
    }

    private static function findCardNumberLabel(
        $cardNumber, 
        $flipedLabels, 
        $stringlength)
    {
        return $flipedLabels[substr($cardNumber, 0, $stringlength)];
    }
}
