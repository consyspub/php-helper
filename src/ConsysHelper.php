<?php
namespace ConsysHelper;

use InvalidArgumentException;

final class ConsysHelper {
    const TEXTO_PURO_HELPER = "texto_puro";
    const ARRAY_HELPER = "array";
    const BANK_HELPER = "bank";

    public static function factory(string $type) : ConsysHelperFormatter {
        switch ($type) {
            case ConsysHelper::TEXTO_PURO_HELPER : 
                return new Factory\TextoPuroHelper();
            break;
            case ConsysHelper::ARRAY_HELPER : 
                return new Factory\ArrayHelper();
            break;
            case ConsysHelper::BANK_HELPER : 
                return new Factory\BankHelper();
            break;
            default :
                throw new InvalidArgumentException('Unknown format given');
            break;
        }
    }
}